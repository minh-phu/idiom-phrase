package minhphu.english.idiomandphrase

import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.google.android.gms.ads.MobileAds
import minhphu.english.idiomandphrase.utils.CipherHelper
import minhphu.english.idiomandphrase.utils.tts.TextToSpeechManager

class MyApplication : MultiDexApplication() {

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        MobileAds.initialize(applicationContext, getString(R.string.app_ads_id))
        TextToSpeechManager.getInstance(this)
        CipherHelper(getSecretKey())
    }

    private external fun getSecretKey(): String

    companion object {
        init {
            System.loadLibrary("native-lib")
        }
    }
}
