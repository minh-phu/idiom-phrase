package minhphu.english.idiomandphrase.data.database.daos

import androidx.room.*
import minhphu.english.idiomandphrase.data.database.entities.Word


@Dao
interface WordDao {

    @Query("SELECT * FROM Word WHERE id = :wordId")
    fun getWordById(wordId: Int): Word

    @Query("SELECT * FROM Word ")
    fun getAllWords(): List<Word>

}