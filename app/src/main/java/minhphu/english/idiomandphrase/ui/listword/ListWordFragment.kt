package minhphu.english.idiomandphrase.ui.listword

import kotlinx.android.synthetic.main.fragment_content_lesson.*
import minhphu.english.idiomandphrase.R
import minhphu.english.idiomandphrase.data.database.entities.Word
import minhphu.english.idiomandphrase.ui.base.BaseFragment
import minhphu.english.idiomandphrase.ui.listword.adapter.ListWordPagerAdapter
import minhphu.english.idiomandphrase.utils.InjectorUtils

class ListWordFragment : BaseFragment() {

    override fun getFragmentLayout(): Int = R.layout.fragment_content_lesson

    override fun updateView() {
        val wordId = ListWordFragmentArgs.fromBundle(arguments!!).wordId
        val listWord = InjectorUtils.provideWordDao(requireContext()).getAllWords()
        val flashCardAdapter = ListWordPagerAdapter(childFragmentManager, listWord)
        vpFlashCard.adapter = flashCardAdapter
        gotoWordSelect(wordId,listWord)
    }

    private fun gotoWordSelect(wordId: Int, listWord: List<Word>) {
        for (i: Int in 0 until listWord.size) {
            if (listWord[i].id == wordId) {
                vpFlashCard.currentItem = i
            }
        }
    }
}

