package minhphu.english.idiomandphrase.ui.search

import android.app.SearchManager
import android.content.Context
import android.view.Menu
import androidx.appcompat.widget.SearchView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_search_word.*
import minhphu.english.idiomandphrase.R
import minhphu.english.idiomandphrase.data.database.AppDatabase
import minhphu.english.idiomandphrase.data.database.entities.Word
import minhphu.english.idiomandphrase.ui.base.BaseFragment
import minhphu.english.idiomandphrase.ui.search.adapter.DictionaryAdapter
import minhphu.english.idiomandphrase.utils.Utils
import java.util.*
import java.util.Collections.sort

class SearchWordsFragment : BaseFragment(), SearchView.OnQueryTextListener,DictionaryAdapter.ItemDictionaryClick {

    private lateinit var dictionaryAdapter: DictionaryAdapter

    private var wordList: List<Word>? = null

    override fun getFragmentLayout(): Int = R.layout.fragment_search_word

    override fun getMenuLayout(): Int = R.menu.menu_search

    override fun updateView() {
        val wordDao = AppDatabase.getInstance(requireContext()).wordDao()
        val words = wordDao.getAllWords()
        sort(
            words
        ) { s1, s2 -> s1.enDecrypt.compareTo(s2.enDecrypt, ignoreCase = true) }
        wordList = words
        dictionaryAdapter = DictionaryAdapter(wordList!!, this)
        rcvSearchWord.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                LinearLayoutManager.VERTICAL
            )
        )
        rcvSearchWord.adapter = dictionaryAdapter
    }

    private fun filter(query: String) {
        val newQuery = query.toLowerCase()
        val filteredModelList = ArrayList<Word>()
        if (wordList != null) {
            for (i in wordList!!.indices) {
                val text = wordList!![i].enDecrypt.toLowerCase()
                if (text.contains(newQuery)) {
                    filteredModelList.add(wordList!![i])
                }
            }
            dictionaryAdapter.animateTo(filteredModelList)
        }
        rcvSearchWord.scrollToPosition(0)
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        val mSearchMenuItem = menu?.findItem(R.id.action_search)
        val searchView = mSearchMenuItem?.actionView as SearchView
        val searchManager =
            requireActivity().getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView.setSearchableInfo(searchManager.getSearchableInfo(requireActivity().componentName))
        searchView.setOnQueryTextListener(this)
    }

    override fun onQueryTextSubmit(query: String?): Boolean = true

    override fun onQueryTextChange(newText: String?): Boolean {
        newText?.let { filter(it) }
        return true
    }

    override fun onDetach() {
        Utils.hideKeyboard(requireActivity())
        super.onDetach()
    }

    override fun onItemDictionaryClick(word: Word) {
        Utils.hideKeyboard(requireActivity())
        val directions = SearchWordsFragmentDirections.toListWordFragment(word.id)
        Navigation.findNavController(rcvSearchWord).navigate(directions)
    }

}