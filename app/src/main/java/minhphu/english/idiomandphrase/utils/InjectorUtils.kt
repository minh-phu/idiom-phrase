package minhphu.english.idiomandphrase.utils

import android.content.Context
import minhphu.english.idiomandphrase.data.database.AppDatabase
import minhphu.english.idiomandphrase.data.database.daos.WordDao

object InjectorUtils {

    fun provideWordDao(context: Context): WordDao {
        return AppDatabase.getInstance(context).wordDao()
    }
}
